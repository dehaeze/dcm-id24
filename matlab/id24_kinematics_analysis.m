%% Clear Workspace and Close figures
clear; close all; clc;

%% Intialize Laplace variable
s = zpk('s');

%% Path for functions, data and scripts
addpath('./mat/'); % Path for data
addpath('./src/'); % Path for functions

%% Colors for the figures
colors = colororder;

% Bragg Angle

% The useful Bragg stroke is between *2* and *35* degrees.

% Typical stroke for an /exafs/ scan is 2 degrees.


%% Bragg angles
bragg = 2:0.01:35; % [deg]

% Beam Energy

% The Bragg law is:
% \begin{equation}
% \boxed{ 2 d \sin(\theta) = n \lambda }
% \end{equation}
% with:
% - $d$ the distance between the lattice planes ($d = 3.133211$ Angstrom for Si111, and $d = 1.044404$ Angstrom for Si311)
% - $\theta$ the Bragg angle
% - $n$ the diffraction order, for us $n = 1$
% - $\lambda$ the wavelength of the output beam

% The energy of the photons can be linked to their wavelength with the Planck equation:
% \begin{equation}
% E = \frac{h c}{\lambda}
% \end{equation}
% where:
% - $h = 6.63 \cdot 10^{-34}\,Js$ is the Planck constant
% - $c$ is the speed of light
% - $E$ is the energy of the light in J

% Therefore, to compute the energy of the beam from the Bragg angle:
% \begin{equation}
% \boxed{ E = \frac{h c}{2 d \sin{\theta}} }
% \end{equation}

% Then, to go from joules to eV:
% \begin{equation}
% 1\,J = 0.625 \cdot 10^{19}\,eV
% \end{equation}

% The relation between the Bragg angle and the Energy of the output beam for the two Crystals is shown in Figure [[fig:energy_fct_bragg]].

% Typically, Si111 is used between 4 and 28 keV and Si311 between 7 and 45 keV.


%% Energy of the output beam as a function of the Bragg angle
figure;
hold on;
plot(bragg, 1e-3*braggToEnergy(bragg, 'si', '111'), 'DisplayName', 'Si 111')
plot(bragg, 1e-3*braggToEnergy(bragg, 'si', '311'), 'DisplayName', 'Si 311')
hold off;
xlabel('Bragg angle [deg]'); ylabel('Beam Energy [keV]');
xlim([bragg(1), bragg(end)]);
set(gca, 'YScale', 'log');
legend('location', 'northeast', 'FontSize', 8, 'NumColumns', 1);

% Total Fast Jack Stroke
% Relation between Bragg and Fast Jack is:
% \begin{equation} \label{eq:bragg_angle_formula}
% \boxed{d_z = \frac{d_{\text{off}}}{2 \cos \theta_b}}
% \end{equation}
% with:
% - $d_{\text{off}}$ the wanted offset between the incident x-ray and the output x-ray
% - $\theta_b$ the bragg angle
% - $d_z$ the corresponding distance between the first and second crystal

% This relation is displayed in Figure [[fig:jack_motion_bragg_angle]].


%% Jack motion as a function of Bragg angle
figure;
plot(bragg, braggToCrystaldistance(bragg, 'd_off', 10))
xlabel('Bragg angle [deg]'); ylabel('Crystal Distance [mm]');
xlim([bragg(1), bragg(end)]);

% Scans with only the piezoelecric actuator?
% As the piezoelectric actuator stroke is only $10\mu m$, it will not be possible to perform large scans with only the piezoelectric actuator.

% The possible energy scan using only the piezoelectric actuators is estimated and shown in Figure [[fig:delta_energy_scan_piezo_111]] for Si111 and in Figure [[fig:delta_energy_scan_piezo_311]] for Si311.


%% Computes Possible Energy scan with only Piezo
fj_stroke = 10e-6; % [m]
energy_stroke_111 = zeros(size(bragg)); % [eV]
xtal_distance = 1e-3*braggToCrystaldistance(bragg); % [m]

for i = 1:length(bragg)
    % Find indice at which point the maximum FJ stroke is reached
    [~, j] = min(abs(xtal_distance - (xtal_distance(i)+fj_stroke)));
    energy_stroke_111(i) = braggToEnergy(bragg(i), 'si', '111') - braggToEnergy(bragg(j), 'si', '111');
end

%% Energy "stroke" when only the piezoelectric actuator is used
figure;
yyaxis left
plot(1e-3*braggToEnergy(bragg, 'si', '111'), 1e-3*energy_stroke_111)
xlabel('Energy $E$ [keV]'); ylabel('Energy Stroke $\Delta E$ [keV]');
set(gca, 'YScale', 'log');
ylim([1e-2, 1e2])

yyaxis right
plot(1e-3*braggToEnergy(bragg, 'si', '111'), energy_stroke_111./braggToEnergy(bragg, 'si', '111'))
ylabel('Energy Stroke $\Delta E / E$ [-]');
set(gca, 'YScale', 'log');
ylim([1e-2, 1e0])

xlim([4, 28])



% #+name: fig:delta_energy_scan_piezo_111
% #+caption: Energy "stroke" when only the piezoelectric actuator is used (Si111)
% #+RESULTS:
% [[file:figs/delta_energy_scan_piezo_111.png]]


%% Computes Possible Energy scan with only Piezo
fj_stroke = 10e-6; % [m]
energy_stroke_311 = zeros(size(bragg)); % [eV]
xtal_distance = 1e-3*braggToCrystaldistance(bragg); % [m]

for i = 1:length(bragg)
    % Find indice at which point the maximum FJ stroke is reached
    [~, j] = min(abs(xtal_distance - (xtal_distance(i)+fj_stroke)));
    energy_stroke_311(i) = braggToEnergy(bragg(i), 'si', '311') - braggToEnergy(bragg(j), 'si', '311');
end

%% Energy "stroke" when only the piezoelectric actuator is used
figure;
yyaxis left
plot(1e-3*braggToEnergy(bragg, 'si', '311'), 1e-3*energy_stroke_311)
xlabel('Energy $E$ [keV]'); ylabel('Energy Stroke $\Delta E$ [keV]');
set(gca, 'YScale', 'log');
ylim([1e-2, 1e2])

yyaxis right
plot(1e-3*braggToEnergy(bragg, 'si', '311'), energy_stroke_311./braggToEnergy(bragg, 'si', '311'))
ylabel('Energy Stroke $\Delta E / E$ [-]');
set(gca, 'YScale', 'log');
ylim([1e-2, 1e0])

xlim([7, 45])

% Fast Jack Velocity - Constant Bragg velocity
% Now let's compute the maximum fast jack velocity for a as a function of Bragg for a maximum Bragg velocity of 3deg/s.

% Suppose Bragg is scanned with constant velocity:
% \begin{equation}
% \theta_b = v_b \cdot t
% \end{equation}
% with $v_b$ the Bragg velocity in [rad/s] and $t$ the time.

% The corresponding velocity of the Fast Jack will be:
% \begin{equation}
% \boxed{d_z^\prime(t) = \frac{d_{\text{off}} \cdot v_b \cdot \tan \theta_b}{2 \cos \theta_b}}
% \end{equation}


%% Maximum Bragg velocity
bragg_vel = 3; % [deg/s]
d_off = 10e-3; % [m]

%% Fast Jack Velocity for a scan at maximum Bragg velocity of 3 deg/s
figure;
hold on;
plot(bragg, 1e6*(d_off*1*pi/180*tan(bragg*pi/180))./(2*cos(bragg*pi/180)), 'DisplayName', '1 deg/s')
plot(bragg, 1e6*(d_off*3*pi/180*tan(bragg*pi/180))./(2*cos(bragg*pi/180)), 'DisplayName', '3 deg/s')
hold off;
xlabel('Bragg angle [deg]');
ylabel('Fast Jack velocity [$\mu$m/s]');
xlim([bragg(1), bragg(end)]);
legend('location', 'northwest', 'FontSize', 8, 'NumColumns', 1);



% #+name: fig:fj_velocity_for_max_bragg_velocity
% #+caption: Fast Jack Velocity for a scan at maximum Bragg velocity of 3 deg/s
% #+RESULTS:
% [[file:figs/fj_velocity_for_max_bragg_velocity.png]]

% The maximum Fast Jack velocity being 1mm/s, this is not an issue.
% However, the Stepper motors in the fast jacks are inducing vibrations with a frequency proportional to their velocity.
% The (spatial) period of the errors is $\delta_\epsilon = 5\mu m$, therefore the frequency of these vibrations is:
% \begin{equation}
% f_\epsilon = \frac{v_{\text{fj}}}{\delta_e}
% \end{equation}
% with:
% - $f_\epsilon$ the frequency of the disturbance in [Hz]
% - $v_{\text{fj}}$ the fast jack velocity in $[\mu m/s]$
% - $\delta_e = 5\,\mu m$ the period of the fast jack errors in $[\mu m]$

% In order to reduce theses induced vibrations, the frequency of these vibrations should be low enough to be in the bandwidth of the feedback controller (piezoelectric actuators).
% With the currently used regulator, the vibrations reduction is better than 50 below 3Hz, better than 10 below 7Hz, and is does nothing above 25Hz.

% To have good results, the frequency of the Fast jack disturbances should therefore be low enough, meaning the fast jack velocity should be small.

% Results are shown in Figure [[fig:frequency_fj_vibrations_fct_bragg]].

% At small Bragg angles (i.e. < 10deg), it is possible to scan with high bragg velocity with no issue.
% At high Bragg angles, it is necessary to reduce the bragg velocity in order to have good results.


%% Frequency of the fast jack disturbances as a function of the Bragg angle
figure;
hold on;
plot(bragg, (1/5)*1e6*(d_off*0.5*pi/180*tan(bragg*pi/180))./(2*cos(bragg*pi/180)), 'DisplayName', '0.5 deg/s')
plot(bragg, (1/5)*1e6*(d_off*1*pi/180*tan(bragg*pi/180))./(2*cos(bragg*pi/180)), 'DisplayName', '1 deg/s')
plot(bragg, (1/5)*1e6*(d_off*2*pi/180*tan(bragg*pi/180))./(2*cos(bragg*pi/180)), 'DisplayName', '2 deg/s')
plot(bragg, (1/5)*1e6*(d_off*3*pi/180*tan(bragg*pi/180))./(2*cos(bragg*pi/180)), 'DisplayName', '3 deg/s')
plot([bragg(1) bragg(end)], [3 3], 'k--', 'DisplayName', '50x reduction')
plot([bragg(1) bragg(end)], [7 7], 'k-.', 'DisplayName', '10x reduction')
plot([bragg(1) bragg(end)], [25 25], 'k:', 'DisplayName', 'no reduction')
hold off;
xlim([bragg(1), bragg(end)]);
xlabel('Bragg angle [deg]');
ylabel('Frequency of disturbances [Hz]');
legend('location', 'northwest', 'FontSize', 8, 'NumColumns', 1);

% Fast Jack Velocity - Constant Energy
% Let's now estimate the frequency of Stepper errors for scans with constant increase of the energy.
% The results are shown in Figure [[fig:frequency_fj_vibrations_fct_energy]] for Si111 and in Figure [[fig:frequency_fj_vibrations_fct_energy_311]] for Si311.


delta_e = 0.01; % Energy between two points [keV]
energy_111 = 1e3*[4:delta_e:28]; % [eV]
xtal_distance_111 = energyToCrystaldistance(energy_111, 'si', '111'); % [m]


velocities_e = [0.002, 0.005, 0.01, 0.02, 0.05, 0.1]; % [keV/s]
freq_fj = zeros(length(energy_111)-1, length(velocities_e));

for i = 1:length(velocities_e)
    velocity_e = velocities_e(i);
    dt = (delta_e/velocity_e); % [s]
    t = dt*[0:length(energy_111)-1]; % Time vector [s]

    vel_fj = (xtal_distance_111(1:end-1) - xtal_distance_111(2:end))/dt; % Fast Jack Velocity [m/s]
    freq_fj(:, i) = (1/5e-6)*vel_fj;
end

%% Frequency of the fast jack disturbances for Energy scans
d_off = 10e-3;
figure;
hold on;
for i = 1:length(velocities_e)
    plot(1e-3*energy_111(2:end), freq_fj(:,i), 'DisplayName', sprintf('%.0f [eV/s]', 1e3*velocities_e(i)))
end
plot([8, 28], [3 3], 'k--', 'DisplayName', '50x reduction')
plot([8, 28], [7 7], 'k-.', 'DisplayName', '10x reduction')
plot([8, 28], [25 25], 'k:', 'DisplayName', 'no reduction')
hold off;
xlim([8, 28]);
ylim([0, 50]);
xlabel('Energy [keV]');
ylabel('Frequency of disturbances [Hz]');
legend('location', 'northeast', 'FontSize', 8, 'NumColumns', 3);



% #+name: fig:frequency_fj_vibrations_fct_energy
% #+caption: Frequency of the fast jack disturbances as a function of the energy for several scan velocities (Si111)
% #+RESULTS:
% [[file:figs/frequency_fj_vibrations_fct_energy.png]]


delta_e = 0.01; % Energy between two points [keV]
energy_311 = 1e3*[7:delta_e:45]; % [eV]
xtal_distance_311 = energyToCrystaldistance(energy_311, 'si', '311'); % [m]


velocities_e = [0.002, 0.005, 0.01, 0.02, 0.05, 0.1]; % [keV/s]
freq_fj = zeros(length(energy_311)-1, length(velocities_e));

for i = 1:length(velocities_e)
    velocity_e = velocities_e(i);
    dt = (delta_e/velocity_e); % [s]
    t = dt*[0:length(energy_311)-1]; % Time vector [s]

    vel_fj = (xtal_distance_311(1:end-1) - xtal_distance_311(2:end))/dt; % Fast Jack Velocity [m/s]
    freq_fj(:, i) = (1/5e-6)*vel_fj;
end

%% Frequency of the fast jack disturbances for Energy scans
d_off = 10e-3;
figure;
hold on;
for i = 1:length(velocities_e)
    plot(1e-3*energy_311(2:end), freq_fj(:,i), 'DisplayName', sprintf('%.0f [eV/s]', 1e3*velocities_e(i)))
end
plot([7, 45], [3 3], 'k--', 'DisplayName', '50x reduction')
plot([7, 45], [7 7], 'k-.', 'DisplayName', '10x reduction')
plot([7, 45], [25 25], 'k:', 'DisplayName', 'no reduction')
hold off;
xlim([7, 45]);
ylim([0, 50]);
xlabel('Energy [keV]');
ylabel('Frequency of disturbances [Hz]');
legend('location', 'northeast', 'FontSize', 8, 'NumColumns', 3);
