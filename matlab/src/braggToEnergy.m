function [energy] = braggToEnergy(bragg, args)
% braggToEnergy -
%
% Syntax: [ref] = braggToEnergy(args)
%
% Inputs:
%    - bragg - Bragg angle in [deg]
%    - si    - Silicon used ['111' or '311']
%
% Outputs:
%    - energy - Energy of the Output beam in [eV]

arguments
    bragg
    args.si (1,1) string {} = "111"
end


h = 6.62607e-34; % [J.s]
c = 2.99792e8; % [m/s]

if strcmp(args.si,'111')
    d = 3.1355e-10; % [m]
elseif strcmp(args.si,'311')
    d = 1.6374e-10; % [m]
end

%% Energy of the output beam [eV]
energy = 6.242e18*(h*c)./(2*d*sin(pi/180*bragg));
