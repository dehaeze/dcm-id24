function [bragg] = crystaldistanceToBragg(xtal_distance, args)
% crystaldistanceToBragg -
%
% Syntax: [ref] = crystaldistanceToBragg(args)
%
% Inputs:
%    - xtal_distance - Distance Between the crystals [mm]
%    - d_off - Offset between the input and output beam [mm]
%
% Outputs:
%    - bragg - Bragg angle in [deg]

arguments
    xtal_distance
    args.d_off (1,1) double {mustBeNumeric} = 10
end

bragg = 180/pi*acos(args.d_off./xtal_distance/2);
