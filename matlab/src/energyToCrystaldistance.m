function [xtal_distance] = energyToCrystaldistance(energy, args)
% energyToCrystaldistance -
%
% Syntax: [ref] = energyToCrystaldistance(args)
%
% Inputs:
%    - energy - Energy of the Output beam in [eV]
%    - d_off - Offset between the input and output beam [mm]
%
% Outputs:
%    - xtal_distance - Distance Between the crystals [mm]

arguments
    energy
    args.d_off (1,1) double {mustBeNumeric} = 10
    args.si    (1,1) string {}              = "111"
end

xtal_distance = args.d_off./(2*cos(energyToBragg(energy, 'si', args.si)*pi/180));
