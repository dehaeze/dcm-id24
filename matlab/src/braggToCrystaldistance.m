function [xtal_distance] = braggToCrystaldistance(bragg, args)
% braggToCrystaldistance -
%
% Syntax: [ref] = braggToCrystaldistance(args)
%
% Inputs:
%    - bragg - Bragg angle in [deg]
%    - d_off - Offset between the input and output beam [mm]
%
% Outputs:
%    - xtal_distance - Distance Between the crystals [mm]

arguments
    bragg
    args.d_off (1,1) double {mustBeNumeric} = 10
end

xtal_distance = args.d_off./(2*cos(bragg*pi/180));
