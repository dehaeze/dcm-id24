function [bragg] = energyToBragg(energy, args)
% energyToBragg -
%
% Syntax: [ref] = energyToBragg(args)
%
% Inputs:
%    - energy - Energy of the Output beam in [eV]
%    - si    - Silicon used ['111' or '311']
%
% Outputs:
%    - bragg - Bragg angle in [deg]

arguments
    energy
    args.si (1,1) string {} = "111"
end

h = 6.62607e-34; % [J.s]
c = 2.99792e8; % [m/s]

if strcmp(args.si,'111')
    d = 3.1355e-10; % [m]
elseif strcmp(args.si,'311')
    d = 1.6374e-10; % [m]
end

%% Energy of the output beam [eV]
bragg = 180/pi*asin((h*c)/(2*d)./(energy/6.242e18));
