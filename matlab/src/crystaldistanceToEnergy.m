function [energy] = crystaldistanceToEnergy(xtal_distance, args)
% crystaldistanceToEnergy -
%
% Syntax: [ref] = crystaldistanceToEnergy(args)
%
% Inputs:
%    - xtal_distance - Distance Between the crystals [mm]
%    - d_off - Offset between the input and output beam [mm]
%    - si    - Silicon used ['111' or '311']
%
% Outputs:
%    - energy - Energy of the Output beam in [eV]

arguments
    xtal_distance
    args.d_off (1,1) double {mustBeNumeric} = 10
    args.si    (1,1) string {}              = "111"
end

energy = braggToEnergy(crystaldistanceToBragg(xtal_distance, 'd_off', args.d_off), 'si', args.si)
